package devcraft;

import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;

public class PostfixCalculatorTest {

    PostfixCalculator calculator = new PostfixCalculator();

    @Test
    public void oneOperandExpressionShouldResultOperand(){
        Assert.assertThat(calculator.calc("0"), is(0.0));
        Assert.assertThat(calculator.calc("10"), is(10.0));
    }

    @Test
    public void twoOperandsPlusExpression(){
        Assert.assertThat(calculator.calc("1,2+"), is(3.0));
        Assert.assertThat(calculator.calc("3,2-"), is(1.0));
        Assert.assertThat(calculator.calc("3,2*"), is(6.0));
        Assert.assertThat(calculator.calc("6,2/"), is(3.0));
    }

    @Test
    public void twoConsecutiveOperators() {
        Assert.assertThat(calculator.calc("1,2,3++"), is(6.0));
    }
}