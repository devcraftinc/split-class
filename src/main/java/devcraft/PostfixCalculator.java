package devcraft;

import java.util.Stack;

public class PostfixCalculator {

    private int i = 0;
    private Double operand;
    private Character operator;
    private StringBuilder numberTokenBuilder = new StringBuilder();
    private Stack<Double> operands = new Stack<>();

    public double calc(String expression) {
        // find the first operand and token.
    	operand = null; 
        operator = null;
        i = 0;
        numberTokenBuilder.setLength(0);
        populateOperandAndOperator(expression, numberTokenBuilder);
        
        // clear all operands operands.
        operands.clear();
        
        
        while (operand != null || operator != null) {
        	// find the next token
            Object token = null;
            if (operand != null) {
                token = operand;
                operand = null;
            } else if (operator != null) {
                token = operator;
                operator = null;
            }
            if (operand == null && operator == null)
                populateOperandAndOperator(expression, numberTokenBuilder);

            if (token instanceof Double)
            	// This is an operand. Push it into the stack.
                operands.push((double) token);
            if (token instanceof Character) {
                // This is an operator. Apply it over the 2 top elements in the stack
            	if ((char) token == '+') {
                    operands.push(operands.pop() + operands.pop());
                } else if ((char) token == '-') {
                    operands.push(-operands.pop() + operands.pop());
                } else if ((char) token == '*') {
                    operands.push(operands.pop() * operands.pop());
                } else if ((char) token == '/') {
                    operands.push(1.0 / operands.pop() * operands.pop());
                }
            }
        }
        return operands.peek();
    }

    private void populateOperandAndOperator(String expression, StringBuilder tokenBuilder) {
        while (i < expression.length() && (operand == null && operator == null)) {
            Character followingChar = populateOperandToken(expression, tokenBuilder);
            operand = takeOperand(tokenBuilder);
            operator = currOperator(followingChar);
            i++;
        }
    }

    private Character populateOperandToken(String expression, StringBuilder tokenBuilder) {
        Character c = expression.charAt(i);
        while (Character.isDigit(c)) {
            tokenBuilder.append(c);
            i++;
            if (i >= expression.length()) {
                return null;
            }
            c = expression.charAt(i);
        }
        return c;
    }

    private Character currOperator(Character c) {
        if (c == null)
            return null;
        if (c == ',')
            return null;
        return c;
    }

    private Double takeOperand(StringBuilder tokenBuilder) {
        Double operand = null;
        if (tokenBuilder.length() > 0) {
            String token = takeToken(tokenBuilder);
            operand = Double.parseDouble(token);
        }
        return operand;
    }

    private String takeToken(StringBuilder tokenBuilder) {
        String token = tokenBuilder.toString();
        tokenBuilder.setLength(0);
        return token;
    }

}